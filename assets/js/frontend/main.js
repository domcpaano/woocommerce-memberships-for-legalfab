jQuery(document).ready(function( $ ){
    $('.wmlf-mobile-toggle-menu').on('click', function() {
        $('.wmlf-app-content').toggleClass('wmlf-app-content_has-active-mobile-sidebar');
    });

    $('.wmlf-article-card').on('click', '.wmlf-post-controls__view-revisions', function( event ) {
        event.preventDefault();
        $( event.delegateTarget ).find('.wmlf-article-card__popup-footer-content-revisions').toggle();
    });

    $('.wmlf-article-card').on('click', '.wmlf-post-controls__view-stats', function( event ) {
        event.preventDefault();
        $( event.delegateTarget ).find('.wmlf-article-card__popup-footer-content-stats').toggle();
    });

    $('.wmlf-hovercard__label').hoverIntent({
    	over: function() {
    		$(this).children('.wmlf-hovercard__pop-up').show();
    	},
    	out: function() {
    		$(this).children('.wmlf-hovercard__pop-up').hide();
    	}
    });

    $('.wmlf-post-controls__bookmark').on('click', function( event ) {
        event.preventDefault()
        var postID = $(this).data('post-id');
        var $this  = $(this);
        
        $.ajax({
            url : wmlf.ajax_url,
            type : 'post',
            data : {
                action : 'wmlf_toggle_bookmark',
                post_id : postID
            },
            success : function( response ) {
                $badgeCounter = $('.badge_my-membership-content-bookmarked');
                badgeCounterVal = parseInt( $badgeCounter.text() );

                if ( 'removed' == response || 'added' == response ) { 
                    $this.toggleClass( 'wmlf-post-controls_bookmarked' );
                }

                if ( 'removed' == response ) {
                    $badgeCounter.text( badgeCounterVal - 1 );
                }

                if ( 'added' == response ) {
                    $badgeCounter.text( badgeCounterVal + 1 );
                }
            }
        });
    });

    $('.wmlf-post-meta-controls').on('click', '.wmlf-post-meta-controls__thumbs-down, .wmlf-post-meta-controls__thumbs-up', function( event ) {
        event.preventDefault()
        var postID = parseInt( $(this).data('post-id') );
        var vote   = parseInt( $(this).data('user-vote') );
        
        var $this  = $(this);
        var $upIndicator  =  $( event.delegateTarget ).find( '.wmlf-post-meta-controls__thumbs-up-indicator' );
        var $downIndicator  =  $( event.delegateTarget ).find( '.wmlf-post-meta-controls__thumbs-down-indicator' );

        var up     = parseInt( $upIndicator.text() );
        var down   = parseInt( $downIndicator.text() );
        
        $.ajax({
            url : wmlf.ajax_url,
            type : 'post',
            data : {
                action : 'wmlf_update_post_votes',
                post_id : postID,
                vote_value: vote
            },
            success : function( response ) {
                var data = response.data;
                $upIndicator.text( data._up_voted );
                $downIndicator.text( data._down_voted );
                console.log( data );
            }
        });
    });
});