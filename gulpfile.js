'use strict';

var watchify = require('watchify');
var browserify = require('browserify');
var gulp = require('gulp');
var source = require('vinyl-source-stream');
var buffer = require('vinyl-buffer');
var gutil = require('gulp-util');
var sourcemaps = require('gulp-sourcemaps');
var assign = require('lodash.assign');
var sass = require('gulp-sass');
var sourcemaps = require('gulp-sourcemaps');
var minify = require('gulp-minify');
var cleanCSS = require('gulp-clean-css');
var rename = require("gulp-rename");
var cssWrap = require('gulp-css-wrap');
var livereload = require('gulp-livereload');
var compass = require('gulp-compass');

// add custom browserify options here
var customOpts = {
  entries: ['./src/index.js'],
  debug: true
};
var opts = assign({}, watchify.args, customOpts);
var b = watchify(browserify(opts));

// add transformations here
// i.e. b.transform(coffeeify);

gulp.task('js', bundle); // so you can run `gulp js` to build the file
b.on('update', bundle); // on any dep update, runs the bundler
b.on('log', gutil.log); // output build logs to terminal

function bundle() {
  return b.bundle()
    // log errors if they happen
    .on('error', gutil.log.bind(gutil, 'Browserify Error'))
    .pipe(source('bundle.js'))
    // optional, remove if you don't need to buffer file contents
    .pipe(buffer())
    // optional, remove if you dont want sourcemaps
    .pipe(sourcemaps.init({loadMaps: true})) // loads map from browserify file
       // Add transformation tasks to the pipeline here.
    .pipe(sourcemaps.write('./')) // writes .map file
    .pipe(gulp.dest('./assets/js/frontend/'));
}

gulp.task('styles', function () {
  return gulp.src('./assets/sass/**/*.scss')
    .pipe(compass({
      config_file: './config.rb',
      css: './assets/css/frontend',
      sass: './assets/sass'
    }))

    // Namespace our entire CSS file so we don't damage the rest of LegalFab's website
    // in case they actually test this plugin.
    .pipe(cssWrap({selector:'.woocommerce-account.logged-in .wmlf'}))

    // Minify our CSS file.
    .pipe(cleanCSS({compatibility: 'ie8'}))
    .pipe(rename({
      suffix: '.min'
    }))

    // Don't make me refresh, Dom is in a hurry.
    .pipe(livereload())
    .pipe(gulp.dest('./assets/css/frontend/'));
});

gulp.task('watch',function() {
    livereload.listen();
    gulp.watch('./assets/sass/**/*.scss',['styles']);
});

