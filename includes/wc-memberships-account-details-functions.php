<?php
/**
 * WooCommerce Memberships
 *
 * @package   WC-Memberships/Classes
 * @author    Dominique Mariano
 * @copyright Copyright (c) 2014-2016, Dominique Mariano
 * @license   http://www.gnu.org/licenses/gpl-3.0.html GNU General Public License v3.0
 */

if ( ! defined( 'ABSPATH' ) ) exit; // Exit if accessed directly

/**
 * Get account details sections
 *
 * @since 1.5.4
 * @return array
 */
function wc_memberships_get_account_details_area_sections() {
	$account_details_area_sections = array();

	if ( has_filter( 'wc_memberships_my_account_my_memberships_title' ) ) {
		// @deprecated hook, use 'wc_memberships_my_memberships_title' instead
		$account_details_area_sections[ 'my-account-memberships' ] = apply_filters( 'wc_memberships_my_account_my_memberships_title', __( 'My Memberships', 'woocommerce-memberships' ) );
	} else {
		$account_details_area_sections[ 'my-account-memberships' ] = apply_filters( 'wc_memberships_my_memberships_title', __( 'My Memberships', 'woocommerce-memberships' ) );
	}

	$account_details_area_sections = array_merge( $account_details_area_sections, array(
		'my-account-downloads' =>  apply_filters( 'woocommerce_my_account_my_downloads_title', __( 'Available Downloads', 'woocommerce' ) ),
		'my-account-orders' =>  apply_filters( 'woocommerce_my_account_my_orders_title', __( 'Order History', 'woocommerce' ) ),
	) );

	// Filters the available choices for the account details area.
	return  apply_filters( 'wc_account_details_area_sections', $account_details_area_sections );
}