<?php
/**
 * WooCommerce Memberships
 *
 * This source file is subject to the GNU General Public License v3.0
 * that is bundled with this package in the file license.txt.
 * It is also available through the world-wide-web at this URL:
 * http://www.gnu.org/licenses/gpl-3.0.html
 * If you did not receive a copy of the license and are unable to
 * obtain it through the world-wide-web, please send an email
 * to license@skyverge.com so we can send you a copy immediately.
 *
 * DISCLAIMER
 *
 * Do not edit or add to this file if you wish to upgrade WooCommerce Memberships to newer
 * versions in the future. If you wish to customize WooCommerce Memberships for your
 * needs please refer to http://docs.woothemes.com/document/woocommerce-memberships/ for more information.
 *
 * @package   WC-Memberships/Classes
 * @author    SkyVerge
 * @copyright Copyright (c) 2014-2016, SkyVerge, Inc.
 * @license   http://www.gnu.org/licenses/gpl-3.0.html GNU General Public License v3.0
 */

if ( ! defined( 'ABSPATH' ) ) exit; // Exit if accessed directly


/**
 * Main function for returning a membership plan
 *
 * @since 1.0.0
 * @param mixed $post Post object or post ID of the membership plan.
 * @return WC_Memberships_Membership_Plan|bool false on failure
 */
function wc_memberships_get_membership_plan( $post = null ) {
	return wc_memberships()->plans->get_membership_plan( $post );
}

/**
 * Main function for returning all available membership plans
 *
 * @since 1.0.0
 * @param array $args Optional array of arguments. Same as for get_posts
 * @return WC_Memberships_Membership_Plan[] Array of WC_Memberships_Membership_Plan objects
 */
function wc_memberships_get_membership_plans( $args = null ) {
	return wc_memberships()->plans->get_membership_plans( $args );
}


/**
 * Get members area sections
 *
 * @since 1.4.0
 * @param int|string $membership_plan Optional: membership plan id for filtering purposes
 * @return array
 */
function wc_memberships_get_members_area_sections( $membership_plan = '' ) {

	// Remove $membership_plan filter for demo purposes, 
	// so LegalFab does not need to have to setup the plugin.
	return apply_filters( 'wc_membership_plan_members_area_sections', array(
		'my-membership-content' 					=> __( 'My Content', 'woocommerce-memberships' ),
		'my-membership-content/new-and-changed'   	=> __( 'New & Changed Content', 'woocommerce-memberships' ),
		//'my-membership-content/coming-soon'   		=> __( 'Coming Soon Content', 'woocommerce-memberships' ),
		'my-membership-content/bookmarked'   		=> __( 'Bookmarked Content', 'woocommerce-memberships' ),
		'my-membership-products' 					=> __( 'My Products', 'woocommerce-memberships' ),
		'my-membership-products/available-now'  	=> __( 'Products Available Now', 'woocommerce-memberships' ),
		'my-membership-products/available-later'  	=> __( 'Products Available Later', 'woocommerce-memberships' ),
		'my-membership-discounts' 					=> __( 'My Discounts', 'woocommerce-memberships' ),
		'my-membership-discounts/available-now' 	=> __( 'My Discounts Available Now', 'woocommerce-memberships' ),
		'my-membership-discounts/available-later' 	=> __( 'My Discounts Available Later', 'woocommerce-memberships' ),
		'my-membership-notes'     					=> __( 'Membership Notes', 'woocommerce-memberships' ),
		'my-membership-notes/read'     				=> __( 'Read Membership Notes', 'woocommerce-memberships' ),
		'my-membership-notes/unread'     			=> __( 'Unread Membership Notes', 'woocommerce-memberships' ),
	) );
}

function wc_memberships_get_members_area_sections_menu_text( $membership_plan = '' ) {
	return apply_filters( 'wc_memberships_get_members_area_sections_menu_text', array(
		'my-membership-content' 	=> __( 'My Contents', 'woocommerce-memberships' ),
		'my-membership-products' 	=> __( 'My Products', 'woocommerce-memberships' ),
		'my-membership-discounts' 	=> __( 'My Discounts', 'woocommerce-memberships' ),
		'my-membership-notes' 		=> __( 'My Plan Notifications', 'woocommerce-memberships' ),
	), $membership_plan );
}

function wc_memberships_get_members_area_sections_sub_menu_text( $membership_plan = '' ) {
	return apply_filters( 'wc_memberships_get_members_area_sections_sub_menu_text', array(
		'my-membership-content/new-and-changed' 	=> __( 'New &amp; Changed', 'woocommerce-memberships' ),
		//'my-membership-content/coming-soon' 		=> __( 'Coming Soon', 'woocommerce-memberships' ),
		'my-membership-content/bookmarked' 			=> __( 'Bookmarked', 'woocommerce-memberships' ),
		'my-membership-products/available-now'		=> __( 'Available Now', 'woocommerce-memberships' ),
		'my-membership-products/available-later' 	=> __( 'Available Later', 'woocommerce-memberships' ),
		'my-membership-discounts/available-now' 	=> __( 'Available Now', 'woocommerce-memberships' ),
		'my-membership-discounts/available-later' 	=> __( 'Available Later', 'woocommerce-memberships' ),
		'my-membership-content' 					=> __( 'All', 'woocommerce-memberships' ),
		'my-membership-products' 					=> __( 'All', 'woocommerce-memberships' ),
		'my-membership-discounts' 					=> __( 'All', 'woocommerce-memberships' ),
		'my-membership-notes' 						=> __( 'All', 'woocommerce-memberships' ),
		'my-membership-notes/read' 					=> __( 'Read', 'woocommerce-memberships' ),
		'my-membership-notes/unread' 				=> __( 'Unread', 'woocommerce-memberships' ),
	), $membership_plan );
}

function wc_memberships_get_members_area_sections_menu_with_badges( $membership_plan = '' ) {
	return apply_filters( 'wc_memberships_get_members_area_sections_menu_with_badges', array(
		'my-membership-content/new-and-changed',
		'my-membership-products/available-now',
		'my-membership-discounts/available-now',
		'my-membership-notes/unread',
	), $membership_plan );
}

function wc_memberships_get_members_area_sections_hierarchy( $membership_plan = '', $section = 'all', $active = 'all', $members_area = array() ) {
	$sections_hierarchy = apply_filters( 'wc_memberships_get_members_area_sections_hierarchy', array(
		'my-membership-content' => array(
			'my-membership-content',
			'my-membership-content/new-and-changed',
			//'my-membership-content/coming-soon',
			'my-membership-content/bookmarked',
		),
		'my-membership-products' => array(
			'my-membership-products',
			'my-membership-products/available-now',
			'my-membership-products/available-later',
		),
		'my-membership-discounts' => array(
			'my-membership-discounts',
			'my-membership-discounts/available-now',
			'my-membership-discounts/available-later',
		),
		'my-membership-notes' => array(
			'my-membership-notes',
			'my-membership-notes/read',
			'my-membership-notes/unread',
		),
	), $membership_plan );

	if ( 'all' === $section ) {
		switch ( $active ) {
			case 'all':
				return $sections_hierarchy;
				break;

			case 'active':
				if ( empty( $members_area ) ) {
					return $sections_hierarchy;
				} else {
					return call_user_func( function( $sections_hierarchy, $members_area ) {
						$all_active = array();

						foreach ( $sections_hierarchy as $section_id => $sub_sections ) {
							if ( ! isset( $all_active[ $section_id ] ) ) {
								$all_active[ $section_id ] = array();
							}

							foreach ( $sub_sections as $key => $sub_section_id ) {
								if ( array_key_exists( $sub_section_id, $members_area ) ) {
									$all_active[ $section_id ][] = $sub_section_id;
								}
							}
						}

						return $all_active;
					}, $sections_hierarchy, $members_area );
				}
				break;
			
			default:
				return $sections_hierarchy;
				break;
		}

	} else {
		if ( array_key_exists( $section, $sections_hierarchy ) ) {
			switch ( $active ) {
				case 'all':
					return $sections_hierarchy[ $section ];
					break;

				case 'active':
					if ( empty( $members_area ) ) {
						return $sections_hierarchy[ $section ];
					} else {
						return call_user_func( function( $sub_section, $members_area ) {
							$active = array();

							foreach ( $sub_section as $sub_section_id ) {
								if ( array_key_exists( $sub_section_id, $members_area ) ) {
									$active[] = $sub_section_id;
								}
							}

							return $active;
						}, $sections_hierarchy[ $section ], $members_area );
					}
					break;
				
				default:
					return $sections_hierarchy[ $section ];
					break;
			}
		}
	}
}

function wc_memberships_get_members_area_sections_parent() {
	
}
