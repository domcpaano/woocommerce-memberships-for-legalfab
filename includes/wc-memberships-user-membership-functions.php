<?php
/**
 * WooCommerce Memberships
 *
 * This source file is subject to the GNU General Public License v3.0
 * that is bundled with this package in the file license.txt.
 * It is also available through the world-wide-web at this URL:
 * http://www.gnu.org/licenses/gpl-3.0.html
 * If you did not receive a copy of the license and are unable to
 * obtain it through the world-wide-web, please send an email
 * to license@skyverge.com so we can send you a copy immediately.
 *
 * DISCLAIMER
 *
 * Do not edit or add to this file if you wish to upgrade WooCommerce Memberships to newer
 * versions in the future. If you wish to customize WooCommerce Memberships for your
 * needs please refer to http://docs.woothemes.com/document/woocommerce-memberships/ for more information.
 *
 * @package   WC-Memberships/Classes
 * @author    SkyVerge
 * @copyright Copyright (c) 2014-2016, SkyVerge, Inc.
 * @license   http://www.gnu.org/licenses/gpl-3.0.html GNU General Public License v3.0
 */

if ( ! defined( 'ABSPATH' ) ) exit; // Exit if accessed directly


/**
 * Main function for returning a user membership
 *
 * Supports getting user membership by membership ID, Post object
 * or a combination of the user ID and membership plan id/slug/Post object.
 *
 * If no $id is provided, defaults to getting the membership for the current user.
 *
 * @since 1.0.0
 * @param mixed $id Optional. Post object or post ID of the user membership, or user ID
 * @param mixed $plan Optional. Membership Plan slug, post object or related post ID
 * @return WC_Memberships_User_Membership
 */
function wc_memberships_get_user_membership( $id = null, $plan = null ) {
	return wc_memberships()->user_memberships->get_user_membership( $id, $plan );
}


/**
 * Get all user membership statuses
 *
 * @since 1.0.0
 * @return array
 */
function wc_memberships_get_user_membership_statuses() {
	return wc_memberships()->user_memberships->get_user_membership_statuses();
}


/**
 * Get the nice name for a user membership status
 *
 * @since  1.0.0
 * @param  string $status
 * @return string
 */
function wc_memberships_get_user_membership_status_name( $status ) {

	$statuses = wc_memberships_get_user_membership_statuses();
	$status   = 'wcm-' === substr( $status, 0, 4 ) ? substr( $status, 4 ) : $status;
	$status   = isset( $statuses[ 'wcm-' . $status ] ) ? $statuses[ 'wcm-' . $status ] : $status;

	return is_array( $status ) && isset( $status['label'] ) ? $status['label'] : $status;
}


/**
 * Get all memberships for a user
 *
 * @since 1.0.0
 * @param int $user_id Optional. Defaults to current user.
 * @param array $args
 * @return WC_Memberships_User_Membership[]|null array of user memberships
 */
function wc_memberships_get_user_memberships( $user_id = null, $args = array() ) {
	return wc_memberships()->user_memberships->get_user_memberships( $user_id, $args );
}


/**
 * Check if user is an active member of a particular membership plan
 *
 * @since 1.0.0
 * @param int $user_id Optional. Defaults to current user.
 * @param int|string $plan Membership Plan slug, post object or related post ID
 * @return bool True, if is an active member, false otherwise
 */
function wc_memberships_is_user_active_member( $user_id = null, $plan ) {
	return wc_memberships()->user_memberships->is_user_active_member( $user_id, $plan );
}


/**
 * Check if user is a member of a particular membership plan
 *
 * @since 1.0.0
 * @param int $user_id Optional. Defaults to current user.
 * @param int|string $plan Membership Plan slug, post object or related post ID
 * @return bool True, if is a member, false otherwise
 */
function wc_memberships_is_user_member( $user_id = null, $plan ) {
	return wc_memberships()->user_memberships->is_user_member( $user_id, $plan );
}


/**
 * Create a new user membership programmatically
 *
 * Returns a new user membership object on success which can then be used to add additional data.
 *
 * @since 1.3.0
 * @param array $args Array of arguments
 * @param string $action Action - either 'create' or 'renew'. When in doubt, use 'create'
 * @return WC_Memberships_User_Membership on success, WP_Error on failure
 */
function wc_memberships_create_user_membership( $args = array(), $action = 'create' ) {

	$defaults = array(
		'user_membership_id' => 0,
		'plan_id'            => 0,
		'user_id'            => 0,
		'product_id'         => 0,
		'order_id'           => 0,
	);

	$args = wp_parse_args( $args, $defaults );

	$data = array(
		'post_parent'    => $args['plan_id'],
		'post_author'    => $args['user_id'],
		'post_type'      => 'wc_user_membership',
		'post_status'    => 'wcm-active',
		'comment_status' => 'open',
	);

	if ( $args['user_membership_id'] > 0 ) {
		$updating   = true;
		$data['ID'] = $args['user_membership_id'];
	} else {
		$updating = false;
	}

	/**
	 * Filter new membership data, used when a product purchase grants access
	 *
	 * @param array $data
	 * @param array $args
	 */
	$data = apply_filters( 'wc_memberships_new_membership_data', $data, array(
		'user_id'    => $args['user_id'],
		'product_id' => $args['product_id'],
		'order_id'   => $args['order_id'],
	) );

	if ( $updating ) {
		$user_membership_id = wp_update_post( $data );
	} else {
		$user_membership_id = wp_insert_post( $data );
	}

	// Bail out on error
	if ( is_wp_error( $user_membership_id ) ) {
		return $user_membership_id;
	}


	// Save/update product and order id that granted access
	if ( $args['product_id'] > 0 ) {
		update_post_meta( $user_membership_id, '_product_id', $args['product_id'] );
	}

	if ( $args['order_id'] > 0 ) {
		update_post_meta( $user_membership_id, '_order_id',   $args['order_id'] );
	}


	// Save/update the membership start date, but only if the membership
	// is not active, ie is not being renewed early.
	if ( 'renew' !== $action ) {
		update_post_meta( $user_membership_id, '_start_date', current_time( 'mysql', true ) );
	}

	// Get the membership plan object so we can calculate end time
	$plan = wc_memberships_get_membership_plan( $args['plan_id'] );

	// Calculate membership end date based on membership length, optionally
	// from the existing end date, if renewing early
	$end_date = '';

	if ( $plan->get_access_length_amount() ) {

		// Early renewals add to the existing membership length, normal
		// cases calculate membership length from now (UTC)
		if ( 'renew' === $action ) {
			$now = get_post_meta( $user_membership_id, '_end_date', true );
		} else {
			$now = current_time( 'mysql', true );
		}

		$end_date = $plan->get_expiration_date( $now, $args );
	}

	// Save/update end date
	$user_membership = wc_memberships_get_user_membership( $user_membership_id );
	$user_membership->set_end_date( $end_date );

	/**
	 * Fires after a user has been granted membership access
	 *
	 * This action hook is similar to wc_memberships_user_membership_saved
	 * but won't fire when memberships are manually created from admin
	 *
	 * @since 1.3.0
	 * @param WC_Memberships_Membership_Plan $membership_plan The plan that user was granted access to
	 * @param array $args
	 */
	do_action( 'wc_memberships_user_membership_created', $plan, array(
		'user_id'            => $args['user_id'],
		'user_membership_id' => $user_membership->get_id(),
		'is_update'          => $updating,
	) );

	return $user_membership;
}


/**
 * Get a user membership from a subscription
 *
 * Returns empty array if no User Memberships are found
 * or Subscriptions is inactive
 *
 * @since 1.5.4
 * @param int|WP_Post $subscription A Subscription post object or id
 * @return \WC_Memberships_User_Membership[] Array of User Membership objects or empty array if none found
 */
function wc_memberships_get_memberships_from_subscription( $subscription ) {

	if ( true !== wc_memberships()->is_subscriptions_active() ) {
		return array();
	}

	$subscriptions = wc_memberships()->get_subscriptions_integration();

	return $subscriptions ? $subscriptions->get_memberships_from_subscription( $subscription ) : array();
}

// This is very bad but I don't have much time.
function wc_get_all_post_ids( $tab = '' ) {
	global $wp_query;

	$current_user = wp_get_current_user();
	$user_id = $current_user->ID;

	$restricted_content_sections = array_keys( wc_memberships_get_members_area_sections() );
	$account_membership_sections = array_keys( wc_memberships_get_account_details_area_sections() );
	$post_ids = array();

	if ( in_array( $tab, $restricted_content_sections, true ) ) {
		if ( isset( $wp_query->query_vars['members_area'] ) && is_account_page() ) {
			$user_id         = get_current_user_id();
			$user_membership = wc_memberships_get_user_membership( $user_id, intval( $wp_query->query_vars['members_area'] ) );

			switch ( $tab ) {
				case 'my-membership-content/coming-soon':
					$restricted_content = $user_membership->get_plan()->get_restricted_content( -1 );

					foreach ( $restricted_content->posts as $member_post ) {
						if ( ! $member_post instanceof WP_Post ) {
							continue;
						}

						$can_view_content = wc_memberships_user_can( $user_id, 'view', array( 'post' => $member_post->ID ) );

						if ( $can_view_content ) {
							continue;
						}

						$post_ids[] = $member_post->ID;
					}
					break;

				case 'my-membership-products':
					$restricted_products = $user_membership->get_plan()->get_restricted_products( -1 );
					
					if ( ! empty( $restricted_products->posts ) ) {
						foreach ( $restricted_products->posts as $member_product ) {
							$product = wc_get_product( $member_product );

							if ( ! $product ) {
								continue;
							}

							$post_ids[] = $product->id;
						}
					}
					break;


				case 'my-membership-products/available-now':
					$restricted_products = $user_membership->get_plan()->get_restricted_products( -1 );
					
					if ( empty( $restricted_products->posts ) ) {
						$count = 0;
					} else {
						foreach ( $restricted_products->posts as $member_product ) {
							$product = wc_get_product( $member_product );

							if ( ! $product ) {
								continue;
							}

							$can_purchase_product = wc_memberships_user_can( $user_id, 'purchase', array( 'product' => $product->id ) );
							
							if ( ! $can_purchase_product  ) {
								continue;
							}

							$post_ids[] = $product->id;
						}
					}
					break;


				case 'my-membership-products/available-later':
					$restricted_products = $user_membership->get_plan()->get_restricted_products( -1 );
					
					if ( empty( $restricted_products->posts ) ) {
						$count = 0;
					} else {
						foreach ( $restricted_products->posts as $member_product ) {
							$product = wc_get_product( $member_product );

							if ( ! $product ) {
								continue;
							}

							$can_purchase_product = wc_memberships_user_can( $user_id, 'purchase', array( 'product' => $product->id ) );
							
							if ( $can_purchase_product  ) {
								continue;
							}

							$post_ids[] = $product->id;
						}
					}
					break;


				// There should be a better way of doing this, without performance drawbacks.
				case 'my-membership-discounts':
					$discounted_products = $user_membership->get_plan()->get_discounted_products( -1 );

					if ( empty( $discounted_products->posts ) ) {
						$count = 0;
					} else {
						foreach ( $discounted_products->posts as $discounted_product ) {
							$product = wc_get_product( $discounted_product );
							if ( ! $product ) {
								continue;
							}

							$can_purchase_product = wc_memberships_user_can( $user_id, 'purchase', array( 'product' => $product->id ) );
							$can_have_discount = wc_memberships_user_has_member_discount( $product->id );

							$show_only_active_discounts = apply_filters( 'wc_memberships_members_area_show_only_active_discounts', true, $user_id, $product->id );

							if ( true === $show_only_active_discounts && ! $can_have_discount ) {
								continue;
							}

							$post_ids[] = $product->id;
						}
					}
					break;


				case 'my-membership-discounts/available-now':
					$discounted_products = $user_membership->get_plan()->get_discounted_products( -1 );

					if ( empty( $discounted_products->posts ) ) {
						$count = 0;
					} else {
						foreach ( $discounted_products->posts as $discounted_product ) {
							$product = wc_get_product( $discounted_product );
							if ( ! $product ) {
								continue;
							}

							$can_purchase_product = wc_memberships_user_can( $user_id, 'purchase', array( 'product' => $product->id ) );
							$can_have_discount = wc_memberships_user_has_member_discount( $product->id );

							if ( ! $can_purchase_product || ! $can_have_discount ) {
								continue;
							}

							$show_only_active_discounts = apply_filters( 'wc_memberships_members_area_show_only_active_discounts', true, $user_id, $product->id );

							if ( true === $show_only_active_discounts && ! $can_have_discount ) {
								continue;
							}

							$post_ids[] = $product->id;
						}
					}
					break;


				case 'my-membership-discounts/available-later':
					$discounted_products = $user_membership->get_plan()->get_discounted_products( -1 );

					if ( empty( $discounted_products->posts ) ) {
						$count = 0;
					} else {
						foreach ( $discounted_products->posts as $discounted_product ) {
							$product = wc_get_product( $discounted_product );
							if ( ! $product ) {
								continue;
							}

							$can_purchase_product = wc_memberships_user_can( $user_id, 'purchase', array( 'product' => $product->id ) );
							$can_have_discount = wc_memberships_user_has_member_discount( $product->id );

							if ( $can_purchase_product &&  $can_have_discount ) {
								continue;
							}

							$show_only_active_discounts = apply_filters( 'wc_memberships_members_area_show_only_active_discounts', true, $user_id, $product->id );

							if ( true === $show_only_active_discounts && ! $can_have_discount ) {
								continue;
							}

							$post_ids[] = $product->id;
						}
					}
					break;

				case 'my-membership-notes/read':
					$notes = $user_membership->get_notes( 'customer', -1, array(
						'meta_query' => array(
							'relation' => 'OR',
							// Do not include notes created BEFORE this plugin is modified for the LegalFab exam.
							array(
								'key' => '_is_read',
								'compare' => 'NOT EXISTS', // works!
   								'value' => '',
							),
							array(
								'key' => '_is_read',
								'value' => 1,
							),
						),
					) );
					
					if ( ! empty( $notes ) ) {
						foreach ( $notes as $note ) {
							$post_ids[] = $note->comment_ID;
						}
					}

					break;


				case 'my-membership-notes/unread':
					$notes = $user_membership->get_notes( 'customer', -1, array(
						'meta_query' => array(
							// Do not include notes created BEFORE this plugin is modified for the LegalFab exam.
							array(
								'key' => '_is_read',
								'value' => 0,
							),
						),
					) );
					
					if ( ! empty( $notes ) ) {
						foreach ( $notes as $note ) {
							$post_ids[] = $note->comment_ID;
						}
					}

					break;

				default:
					break;
			}
		}
	}

	return $post_ids;
}


function wc_get_badge_count( $tab = '' ) {
	global $wp_query;

	$current_user = wp_get_current_user();
	$user_id = $current_user->ID;

	$restricted_content_sections = array_keys( wc_memberships_get_members_area_sections() );
	$account_membership_sections = array_keys( wc_memberships_get_account_details_area_sections() );
	$count = 0;

	if ( in_array( $tab, $restricted_content_sections, true ) ) {
		if ( isset( $wp_query->query_vars['members_area'] ) && is_account_page() ) {
			$user_membership = wc_memberships_get_user_membership( $user_id, intval( $wp_query->query_vars['members_area'] ) );

			switch ( $tab ) {
				case 'my-membership-content':
					$restricted_content = $user_membership->get_plan()->get_restricted_content( -1 );
					$count = ! empty ( $restricted_content->found_posts ) ? $restricted_content->found_posts : 0;
					break;


				case 'my-membership-content/new-and-changed':
					// Remember to show in the cards when content is viewable in full or not!
					$restricted_content = $user_membership->get_plan()->get_restricted_content_new_and_changed( -1 );
					$count = ! empty ( $restricted_content->found_posts ) ? $restricted_content->found_posts : 0;
					break;


				case 'my-membership-content/coming-soon':
					$restricted_content = $user_membership->get_plan()->get_restricted_content( -1 );

					if ( ! empty ( $restricted_content ) ) {
						foreach ( $restricted_content->posts as $member_post ) {
							if ( ! $member_post instanceof WP_Post ) {
								continue;
							}

							$can_view_content = wc_memberships_user_can( $user_id, 'view', array( 'post' => $member_post->ID ) );

							if ( $can_view_content ) {
								continue;
							}

							$count = $count + 1;
						}
					}
					break;


				case 'my-membership-content/bookmarked':
					$restricted_content = $user_membership->get_plan()->get_restricted_content_bookmarked( -1 );
					$count = ! empty ( $restricted_content->found_posts ) ? $restricted_content->found_posts : 0;
					break;


				case 'my-membership-products':
					$restricted_products = $user_membership->get_plan()->get_restricted_products( -1 );
					
					if ( empty( $restricted_products->posts ) ) {
						$count = 0;
					} else {
						foreach ( $restricted_products->posts as $member_product ) {
							$product = wc_get_product( $member_product );

							if ( ! $product ) {
								continue;
							}

							$count = $count + 1;
						}
					}
					break;


				case 'my-membership-products/available-now':
					$restricted_products = $user_membership->get_plan()->get_restricted_products( -1 );
					
					if ( empty( $restricted_products->posts ) ) {
						$count = 0;
					} else {
						foreach ( $restricted_products->posts as $member_product ) {
							$product = wc_get_product( $member_product );

							if ( ! $product ) {
								continue;
							}

							$can_purchase_product = wc_memberships_user_can( $user_id, 'purchase', array( 'product' => $product->id ) );
							
							if ( ! $can_purchase_product  ) {
								continue;
							}

							$count = $count + 1;
						}
					}
					break;


				case 'my-membership-products/available-later':
					$restricted_products = $user_membership->get_plan()->get_restricted_products( -1 );
					
					if ( empty( $restricted_products->posts ) ) {
						$count = 0;
					} else {
						foreach ( $restricted_products->posts as $member_product ) {
							$product = wc_get_product( $member_product );

							if ( ! $product ) {
								continue;
							}

							$can_purchase_product = wc_memberships_user_can( $user_id, 'purchase', array( 'product' => $product->id ) );
							
							if ( $can_purchase_product  ) {
								continue;
							}

							$count = $count + 1;
						}
					}
					break;


				// There should be a better way of doing this, without performance drawbacks.
				case 'my-membership-discounts':
					$discounted_products = $user_membership->get_plan()->get_discounted_products( -1 );

					if ( empty( $discounted_products->posts ) ) {
						$count = 0;
					} else {
						foreach ( $discounted_products->posts as $discounted_product ) {
							$product = wc_get_product( $discounted_product );
							if ( ! $product ) {
								continue;
							}

							$can_purchase_product = wc_memberships_user_can( $user_id, 'purchase', array( 'product' => $product->id ) );
							$can_have_discount = wc_memberships_user_has_member_discount( $product->id );

							$show_only_active_discounts = apply_filters( 'wc_memberships_members_area_show_only_active_discounts', true, $user_id, $product->id );

							if ( true === $show_only_active_discounts && ! $can_have_discount ) {
								continue;
							}

							$count = $count + 1;
						}
					}
					break;


				case 'my-membership-discounts/available-now':
					$discounted_products = $user_membership->get_plan()->get_discounted_products( -1 );

					if ( empty( $discounted_products->posts ) ) {
						$count = 0;
					} else {
						foreach ( $discounted_products->posts as $discounted_product ) {
							$product = wc_get_product( $discounted_product );
							if ( ! $product ) {
								continue;
							}

							$can_purchase_product = wc_memberships_user_can( $user_id, 'purchase', array( 'product' => $product->id ) );
							$can_have_discount = wc_memberships_user_has_member_discount( $product->id );

							if ( ! $can_purchase_product || ! $can_have_discount ) {
								continue;
							}

							$show_only_active_discounts = apply_filters( 'wc_memberships_members_area_show_only_active_discounts', true, $user_id, $product->id );

							if ( true === $show_only_active_discounts && ! $can_have_discount ) {
								continue;
							}

							$count = $count + 1;
						}
					}
					break;


				case 'my-membership-discounts/available-later':
					$discounted_products = $user_membership->get_plan()->get_discounted_products( -1 );

					if ( empty( $discounted_products->posts ) ) {
						$count = 0;
					} else {
						foreach ( $discounted_products->posts as $discounted_product ) {
							$product = wc_get_product( $discounted_product );
							if ( ! $product ) {
								continue;
							}

							$can_purchase_product = wc_memberships_user_can( $user_id, 'purchase', array( 'product' => $product->id ) );
							$can_have_discount = wc_memberships_user_has_member_discount( $product->id );

							if ( $can_purchase_product &&  $can_have_discount ) {
								continue;
							}

							$show_only_active_discounts = apply_filters( 'wc_memberships_members_area_show_only_active_discounts', true, $user_id, $product->id );

							if ( true === $show_only_active_discounts && ! $can_have_discount ) {
								continue;
							}

							$count = $count + 1;
						}
					}
					break;


				case 'my-membership-notes':
					$notes = $user_membership->get_notes( 'customer', -1 );
					$count = count( $notes );
					break;


				case 'my-membership-notes/read':
					$notes = $user_membership->get_notes( 'customer', -1, array(
						'meta_query' => array(
							'relation' => 'OR',
							// Do not include notes created BEFORE this plugin is modified for the LegalFab exam.
							array(
								'key' => '_is_read',
								'compare' => 'NOT EXISTS', // works!
   								'value' => '',
							),
							array(
								'key' => '_is_read',
								'value' => 1,
							),
						),
					) );
					$count = count( $notes );
					break;


				case 'my-membership-notes/unread':
					$notes = $user_membership->get_notes( 'customer', -1, array(
						'meta_query' => array(
							// Do not include notes created BEFORE this plugin is modified for the LegalFab exam.
							array(
								'key' => '_is_read',
								'value' => 0,
							),
						),
					) );
					$count = count( $notes );
					break;

				default:
					break;
			}
		}
	} elseif ( in_array( $tab, $account_membership_sections, true ) ) {
		switch ( $tab ) {
			case 'my-account-memberships':
				$count = count( wc_memberships_get_user_memberships() );
				break;
			case 'my-account-downloads':
				$downloads = WC()->customer->get_downloadable_products();
				$count = count( $downloads );
				break;
			case 'my-account-orders':
				$customer_orders = get_posts( array(
					'numberposts' => -1,
					'meta_key'    => '_customer_user',
					'meta_value'  => get_current_user_id(),
					'post_type'   => wc_get_order_types( 'view-orders' ),
					'post_status' => array_keys( wc_get_order_statuses() ),
				) );
				$count = count( $customer_orders );
				break;
			default:
				break;
		}
	}

	return $count;
}
