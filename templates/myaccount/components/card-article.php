<?php
if ( ! defined( 'ABSPATH' ) ) { exit; // Exit if accessed directly
}

if ( ! $member_post instanceof WP_Post ) {
	exit;
}

if ( empty( $post_controls ) || ! is_array( $post_controls ) ) {
	$post_controls = array();
}

if ( empty( $post_meta_controls ) || ! is_array( $post_meta_controls ) ) {
	$post_meta_controls = array();
}



$current_user = wp_get_current_user();
$user_id = $current_user->ID;
$can_view_content = wc_memberships_user_can( $user_id, 'view', array( 'post' => $member_post->ID ) );
$view_start_time  = wc_memberships_get_user_access_start_time( $user_id, 'view', array( 'post' => $member_post->ID ) );
?>

<article class="wmlf-article-card">
	<div class="wmlf-article-card__body">
		<div class="wmlf-article-card__content">
			
			<?php if ( $can_view_content ): ?>
				<a href="<?php echo esc_url( get_permalink( $member_post->ID ) ); ?>" class="wmlf-article-card__title-link">
					<h4 class="wmlf-article-card__title"><?php echo esc_html( get_the_title( $member_post->ID ) ); ?></h4>
				</a>
			<?php else: ?>
				<h4 class="wmlf-article-card__title"><?php echo esc_html( get_the_title( $member_post->ID ) ); ?></h4>
			<?php endif; ?>
			
			<div class="wmlf-article-card__post-excerpt">
				<?php if ( empty( $member_post->post_excerpt ) ) : ?>
					<?php echo wp_kses_post( wp_trim_words( strip_shortcodes( $member_post->post_content ), 50 ) ); ?>
				<?php else : ?>
					<?php echo wp_kses_post( wp_trim_words( $member_post->post_excerpt, 50 ) ); ?>
				<?php endif; ?>
			</div><!-- .wmlf-article-card__post-excerpt -->
		</div><!-- .wmlf-article-card__content -->

		<footer class="wmlf-article-card__info clear">
			<p class="wmlf-article-card__post-relative-time-status">
				<?php wmlf_get_svg( 'clock' ); ?>

				<?php if ( $can_view_content ): ?>
					<span class="time">
						<span class="time-text"><?php echo esc_html( sprintf( __( 'Post published on %s.', 'woocommerce-memberships' ), date_i18n( get_option( 'date_format' ), strtotime( $member_post->post_date ) ) ) ); ?> <?php echo esc_html( sprintf( __( 'Last updated on %s.', 'woocommerce-memberships' ), date_i18n( get_option( 'date_format' ), strtotime( $member_post->post_modified ) ) ) ); ?></span>
					</span>
				<?php else: ?>

					<span class="time">
						<span class="time-text"><?php _e( 'Content will be accessible on ', 'woocommerce-memberships' ); ?><time datetime="<?php echo date( 'Y-m-d', $view_start_time ); ?>" title="<?php echo esc_attr( $view_start_time ); ?>"><?php echo date_i18n( get_option( 'date_format' ), $view_start_time ); ?></time></span>
					</span>
				<?php endif; ?>
			</p>
			
			<?php if ( ! empty( $post_meta_controls ) ) : ?>
			<ul class="wmlf-post-meta-controls">
				<?php foreach ( $post_meta_controls as $control => $values ) : ?>
					<?php if ( 'vote-up' === $control ) : ?>
					<li>
						<a data-user-vote="1" data-post-id="<?php echo esc_attr( $member_post->ID ); ?>" class="wmlf-post-meta-controls__control wmlf-post-meta-controls__thumbs-up" title="<?php _e( 'Vote this article as helpful.', 'woocommerce-memberships' ); ?>">
							<?php wmlf_get_svg( 'thumbs-up' ); ?>

							<span class="wmlf-post-meta-controls__vote-indicator wmlf-post-meta-controls__thumbs-up-indicator"><?php echo esc_html( absint( get_post_meta( $member_post->ID, '_up_voted', true ) ) ); ?></span> 
						</a>
					</li>
					<?php endif; ?>

					<?php if ( 'vote-down' === $control ) : ?>
					<li>
						<a data-user-vote="-1" data-post-id="<?php echo esc_attr( $member_post->ID ); ?>" class="wmlf-post-meta-controls__control wmlf-post-meta-controls__thumbs-down" title="<?php _e( 'Vote this article as unhelpful.', 'woocommerce-memberships' ); ?>">
							<?php wmlf_get_svg( 'thumbs-down' ); ?>
							<span class="wmlf-post-meta-controls__vote-indicator wmlf-post-meta-controls__thumbs-down-indicator"><?php echo esc_html( absint( get_post_meta( $member_post->ID, '_down_voted', true ) ) ); ?></span>
						</a>
					</li>
					<?php endif; ?>
				<?php endforeach; ?>

				<?php unset( $control, $values ); ?>
			</ul>
			<?php endif; ?>
		</footer>
	</div><!-- .wmlf-article-card__body -->
	
	<?php if ( ! empty( $post_controls ) ) : ?>
	<div class="wmlf-post-controls">
		<ul class="wmlf-post-controls__pane post-controls__main-options">
			<?php foreach ( $post_controls as $control => $values ) : ?>

				<?php if ( 'view' === $control ) : ?>
				<li>
					<?php if ( $can_view_content ): ?>
						<a href="<?php echo esc_url( get_permalink( $member_post->ID ) ); ?>" class="wmlf-post-controls__control wmlf-post-controls__view" target="_blank" title="<?php _e( 'View', 'woocommerce-memberships' ); ?>">
							<?php wmlf_get_svg( 'external-link' ); ?>
							<span class="wmlf-post-controls__control-text"><?php _e( 'View', 'woocommerce-memberships' ); ?></span>
						</a>
					<?php else: ?>
						<span href="<?php ?>" class="wmlf-post-controls__control wmlf-post-controls__edit wmlf-post-controls__control_disabled">
							<?php wmlf_get_svg( 'external-link' ); ?>
							<span class="wmlf-post-controls__control-text" title="<?php _e( 'View', 'woocommerce-memberships' ); ?>"><?php _e( 'View', 'woocommerce-memberships' ); ?></span>
						</span>
					<?php endif; ?>

				</li>
				<?php endif; ?>
				
				<?php if ( 'view-revisions' === $control ) : ?>
				<li>
					<a class="wmlf-post-controls__control wmlf-post-controls__view-revisions" target="_blank" title="<?php _e( 'Changes', 'woocommerce-memberships' ); ?>">
						<?php wmlf_get_svg( 'revisions' ); ?>
						<span class="wmlf-post-controls__control-text"><?php _e( 'Changes', 'woocommerce-memberships' ); ?></span>
					</a>
				</li>
				<?php endif; ?>
		
				<?php if ( 'view-stats' === $control ) : ?>
				<li>
					<a class="wmlf-post-controls__control wmlf-post-controls__view-stats" title="<?php _e( 'Stats', 'woocommerce-memberships' ); ?>">
						<?php wmlf_get_svg( 'bar-chart' ); ?>
						<span class="wmlf-post-controls__control-text"><?php _e( 'Stats', 'woocommerce-memberships' ); ?></span>
					</a>
				</li>
				<?php endif; ?>

				<?php if ( 'bookmark' === $control ) : ?>
				<li>
					<?php 
						$bookmarked = false;
						$bookmarked_list = get_user_meta( $user_id, '_bookmarked', true ); // list of bookmarked content for this user
						if ( ! empty( $bookmarked_list ) && in_array( (string) $member_post->ID, $bookmarked_list, true ) ) {
							$bookmarked = true;
						}
					?>
					<a class="wmlf-post-controls__control wmlf-post-controls__bookmark <?php echo esc_attr( ( ! empty( $bookmarked ) ) ? 'wmlf-post-controls_bookmarked' : 'wmlf-post-controls_not-bookmarked' ); ?>" data-post-id="<?php echo esc_attr( $member_post->ID ); ?>" title="<?php echo esc_attr( ( ! empty( $bookmarked ) ) ? __( 'Remove from bookmarks.', 'woocommerce-memberships' ) : __( 'Add to bookmarks.', 'woocommerce-memberships' ) ); ?>">
						<span class="wmlf-post-controls__bookmark-button wmlf-post-controls__bookmark-button-add"><?php wmlf_get_svg( 'bookmark-add' ); ?></span>
						<span class="wmlf-post-controls__bookmark-button wmlf-post-controls__bookmark-button-remove"><?php wmlf_get_svg( 'bookmark-remove' ); ?></span>
						<span class="wmlf-post-controls__control-text"><?php _e( 'Bookmark', 'woocommerce-memberships' ); ?></span>
					</a>
				</li>
				<?php endif; ?>

			<?php endforeach; ?>
		</ul>
	</div><!-- .wmlf-post-controls -->
	<?php endif; ?>

	<div class="wmlf-article-card__popup-footer-content wmlf-article-card__popup-footer-content-revisions">
		<?php esc_html_e( "Article revisions. Woops, it looks like my developer Dom hasn't implemented me yet.", 'woocommerce-memberships' ); ?>
	</div>

	<div class="wmlf-article-card__popup-footer-content wmlf-article-card__popup-footer-content-stats">
		<?php esc_html_e( "Viewing statistics. Woops, it looks like my developer Dom hasn't implemented me yet.", 'woocommerce-memberships' ); ?>
	</div>
</article><!-- .wmlf-article-card -->
