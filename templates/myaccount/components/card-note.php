<?php
if ( ! defined( 'ABSPATH' ) ) { 
	exit; // Exit if accessed directly
}

if ( empty( $note ) ) {
	exit;
}
?>

<article class="wmlf-article-card">
	<div class="wmlf-article-card__body">
		<div class="wmlf-article-card__content">
			<h4 class="wmlf-article-card__title"><?php echo $note->comment_author; ?></h4>
			
			<div class="wmlf-article-card__post-excerpt">
				<?php echo $note->comment_content; ?>
			</div><!-- .wmlf-article-card__post-excerpt -->
		</div><!-- .wmlf-article-card__content -->

		<footer class="wmlf-article-card__info clear">
			<p class="wmlf-article-card__post-relative-time-status">
				<?php wmlf_get_svg( 'clock' ); ?>

				<span class="time">
					<span class="time-text"><?php echo $note->comment_date; ?></span>
					<span class="time-text"></span>
				</span>
			</p>

			<ul class="wmlf-article-card__meta">
			</ul>
		</footer>
	</div><!-- .wmlf-article-card__body -->
</article><!-- .wmlf-article-card -->
