<?php
if ( ! defined( 'ABSPATH' ) ) {
	exit; // Exit if accessed directly
}

?>

<article class="wmlf-article-card">
	<div class="wmlf-article-card__body">
		<div class="wmlf-article-card__content">
			<?php if ( ! empty( $title ) ): ?>
				<h4 class="wmlf-article-card__title"><?php echo $title; ?></h4>
			<?php endif; ?>

			<?php if ( ! empty( $excerpt ) ) : ?>
				<div class="wmlf-article-card__post-excerpt wmlf-article-card__post-content">
						<?php echo $excerpt; ?>
				</div><!-- .wmlf-article-card__post-excerpt -->
			<?php elseif ( ! empty( $content ) ): ?>
				<div class="wmlf-article-card__post-full-content wmlf-article-card__post-content">
						<?php echo $content; ?>
				</div><!-- .wmlf-article-card__post-excerpt -->
			<?php endif; ?>

		</div><!-- .wmlf-article-card__content -->

		<footer class="wmlf-article-card__info clear">
			<p class="wmlf-article-card__post-relative-time-status">
				<?php if ( ! empty( $post_date ) || ! empty( $post_last_modified_date ) || ! empty( $post_last_seen_date ) ) : ?>
					<?php wmlf_get_svg( 'clock' ); ?>
				<?php endif; ?>

				<span class="time">
					<?php if ( ! empty( $post_date ) ) : ?>
					<span class="time-text time-text_posted" title="">
						<?php echo $post_date; ?>
					</span>
					<?php endif; ?>

					<?php if ( ! empty( $post_last_modified_date ) ) : ?>
						<span class="time-text time-text_last-modified" title="<?php echo esc_attr( $post_last_modified_date ); ?>">
							<?php echo est_html( $post_last_modified_date ); ?>
						</span>
					<?php endif; ?>

					<?php if ( ! empty( $post_last_seen_date ) ) : ?>
						<span class="time-text time-text_last-modified" title="<?php echo esc_attr( $post_last_seen_date ); ?>">
							<?php printf( _x( '%s ago.', '%s = human-readable time difference', 'woocommerce-memberships' ), human_time_diff( strtotime( $post_last_seen_date ), current_time( 'timestamp' ) ) ); ?>
						</span>
					<?php endif; ?>

				</span>
			</p>
		</footer>
	</div><!-- .wmlf-article-card__body -->
</article><!-- .wmlf-article-card -->
