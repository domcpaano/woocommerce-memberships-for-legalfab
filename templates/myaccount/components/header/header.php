<div class="wmlf-masthead">
	<div class="wmlf-bar-primary">
		
		<button class="wmlf-mobile-toggle-menu">
			<?php wmlf_get_svg( 'menu' ); ?>
		</button>

		<?php wmlf_get_template_component( 'hovercard-notifications.php' ); ?>

		<?php wmlf_get_template_component( 'hovercard-edit-settings.php' ); ?>

	</div><!-- .wmlf-profile-bar -->

	<div class="wmlf-bar-secondary">
		<?php woocommerce_breadcrumb(); ?>
	</div>	
</div>
