<ul class="wmlf-hovercard wmlf-hovercard_edit-settings">
	<li class="wmlf-hovercard__label">

		<?php $current_user = wp_get_current_user(); ?>
	
		<a class="wmlf-hovercard__label-link wmlf-hovercard__label-link_edit-settings" href="<?php echo wc_customer_edit_account_url(); ?>">
			<span class="wmlf-hovercard__profile-name">
				<?php echo esc_html( $current_user->display_name ); ?>
			</span> 
			<?php echo get_avatar( $current_user->ID, 26, 'mysteryman', false, array(
				'height' => 26,
				'width' => 26,
				'class' => 'wmlf-hovercard__profile-picture vertical-center',
			) ); ?>
		</a><!-- .wmlf-profile-bar__profile-bar-label-link -->

		<div class="wmlf-hovercard__pop-up wmlf-hovercard__pop-up_edit-settings">
			<p class="wmlf-hovercard__description"><?php echo esc_html( __( 'Manage your account.', 'woocommerce-memberships' ) ); ?></p>
			
			<ul class="wmlf-hovercard__menu">
				<li  class="wmlf-hovercard__menu-item">
					<a href="<?php echo wc_customer_edit_account_url(); ?>"><?php _e( 'Edit Profile', 'woocommerce-memberships' ); ?></a>
				</li>
				<li class="wmlf-hovercard__menu-item">
					<a href="<?php echo wc_get_endpoint_url( 'edit-address', 'billing' ); ?>"><?php _e( 'Add/Change Billing Address', 'woocommerce-memberships' ); ?></a>
				</li>

				<?php if ( ! wc_ship_to_billing_address_only() && get_option( 'woocommerce_calc_shipping' ) !== 'no' ) : ?>
				<li class="wmlf-hovercard__menu-item">
					<a href="<?php echo wc_get_endpoint_url( 'edit-address', 'shipping' ); ?>"><?php _e( 'Add/Change Shipping Address', 'woocommerce-memberships' ); ?></a>
				</li>
				<?php endif; ?>
	
				<li class="wmlf-hovercard__menu-item">
					<a href="<?php echo esc_url( wc_memberships_get_account_details_area_url() ); ?>"><?php _e( 'Update Plans & Subscriptions', 'woocommerce-memberships' ); ?></a>
				</li>
			</ul><!-- #user_bar_user_actions -->

			<a class="log-out-link" href="<?php echo wc_get_endpoint_url( 'customer-logout', '', wc_get_page_permalink( 'myaccount' ) ); ?>">Log Out</a>
		</div><!-- .wmlf-profile-bar__profile-bar-drop-down-pop-up -->
	</li><!-- .wmlf-profile-bar__profile-bar-label -->
</ul><!-- .wmlf-hovercard -->
