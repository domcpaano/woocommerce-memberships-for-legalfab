<ul class="wmlf-hovercard wmlf-hovercard__notification">
	<li class="wmlf-hovercard__label">

		<?php 
			$current_user = wp_get_current_user();
			$customer_memberships = wc_memberships_get_user_memberships();
			
			$all_plan_notes = array();

			if ( ! empty( $customer_memberships ) ) {
				foreach ( $customer_memberships as $customer_membership ) {
					$customer_notes = $customer_membership->get_notes( 'customer', -1, array(
						'meta_query' => array(
							// Do not include notes created BEFORE this plugin is modified for the LegalFab exam.
							array(
								'key' => '_is_read',
								'value' => 0,
							),
						),
					) );

					foreach ( $customer_notes as $note ) {
						$all_plan_notes[] = array( $customer_membership->get_plan_id(), $note );
					}
				}
			}

			$comment_count = '';
			if ( ! empty( $all_plan_notes ) ) {
				$comment_count = count( $all_plan_notes );
			}

			unset( $note );
		?>
	
		<a class="wmlf-hovercard__label-link">
			<span class="notification vertical-center vertical-center_align-block">
				<?php wmlf_get_svg( 'bell' ); ?>
				<?php if ( ! empty( $comment_count ) ) : ?>
					<i class="badge badge_alert notification__count"><?php echo esc_html( $comment_count ); ?></i>
				<?php endif; ?>
			</span>
		</a>

		<div class="wmlf-hovercard__pop-up wmlf-hovercard__pop-up_notifications">
			<p class="wmlf-hovercard__description">
				<?php if ( ! $comment_count ): ?>
					<?php _e( 'You have no new notifications.', 'woocommerece-memberships' ); ?>
				<?php else: ?>
					<?php printf( esc_html( _n( 'You have %d new notification.', 'You have %d new notifications.', $comment_count, 'woocommerece-memberships' ) ), $comment_count ); ?>
				<?php endif; ?>
			</p>
		
			<?php if ( ! empty( $all_plan_notes ) ) : ?>	
			<ul class="wmlf-hovercard__menu">
				<?php foreach ( $all_plan_notes as $note ) : ?>
				<li  class="wmlf-hovercard__menu-item">
					<a href="<?php echo esc_url( wc_memberships_get_members_area_url( $note[0], 'my-membership-notes/note', $note[1]->comment_ID ) ); ?>">
						<span><?php //print_r( $note ); ?></span>
						<span><?php echo esc_html( $note[1]->comment_date ); ?></span>
						<span><?php echo esc_html( substr( $note[1]->comment_content, 0, 50 ) ); ?></span>
					</a>
				</li>
				<?php endforeach; ?>
			</ul><!-- #user_bar_user_actions -->
			<?php endif; ?>
		</div><!-- .wmlf-profile-bar__edit-settings-drop-down-pop-up -->
	</li><!-- .wmlf-profile-bar__edit-settings-label -->
</ul><!-- .wmlf-hovercard -->
