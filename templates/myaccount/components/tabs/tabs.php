<?php if ( ! empty( $tabs ) ) : ?>
<div class="wmlf-tabs">
	<ul class="wmlf-tabs__list" role="menu">
		<?php if ( is_string( $tabs ) ) :  ?>
			<li class="wmlf-tabs__tab wmlf-tabs__tab_selected">
				<a href="" class="wmlf-tabs__link" tabindex="0" aria-selected="true" role="menuitem">
					<h2 class="wmlf-tabs__text"><?php echo esc_html( $tabs ); ?></h2>
				</a>
			</li><!-- .wmlf-tab -->
		<?php elseif ( is_array( $tabs ) ) : ?>
			<?php foreach ( $tabs as $tab ) : ?>		
				<?php $current_section_class = ''; ?>

				<?php
					if ( ! empty( $current_section ) && ! empty( $current_sub_section ) ) {
						if ( ( $current_section . '/' . $current_sub_section ) === $tab ) {
							$current_section_class = 'wmlf-tabs__tab_selected';
						}
					} else if ( ! empty( $current_section ) && empty( $current_sub_section ) ) {
						if ( $current_section == $tab ) {
							$current_section_class = 'wmlf-tabs__tab_selected';
						}
					}
				?>

				<li class="wmlf-tabs__tab <?php echo esc_attr( $current_section_class ); ?>">
					<?php
						$url = '';

						if ( isset( $customer_membership ) && is_object( $customer_membership ) && method_exists( $customer_membership, 'get_plan_id' ) ) {
							$url = wc_memberships_get_members_area_url( $customer_membership->get_plan_id(), $tab );
						} else {
							$url = wc_memberships_get_account_details_area_url( $tab );
						}
					?>
					<a href="<?php echo esc_url( $url ); ?>" class="wmlf-tabs__link" tabindex="0" aria-selected="true" role="menuitem">
						<span class="wmlf-tabs__text">
							<?php echo esc_html( $section_labels[ $tab ] ); ?>
						</span>
						
						<?php
							$badge_classes = array(); 
							$badge_count = absint( wc_get_badge_count( $tab ) );

							if ( ! empty( $section_badges ) && is_array( $section_badges ) && in_array( $tab, $section_badges ) ) {
								$badge_classes[] = ( ! empty( $badge_count ) ) ? 'badge_important' : '';
							}

							$badge_classes[] = "badge_" . str_replace( '/', '-', $tab );
						?>

						<span class="badge <?php echo esc_attr( implode( ' ', $badge_classes ) ); ?>"><?php echo esc_html( $badge_count ); ?></span>
					</a>
				</li><!-- .wmlf-tab -->
			<?php endforeach; ?>
		<?php endif; ?>
	</ul><!-- .wmlf-tabs__list -->
</div><!-- .wmlf-tabs -->
<?php endif; ?>
