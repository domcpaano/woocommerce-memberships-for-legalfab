<?php
/**
 * WooCommerce Memberships
 *
 * @author    Dominique A. Mariano
 * @copyright Copyright (c) 2014-2016, Dominique A. Mariano
 * @license   http://www.gnu.org/licenses/gpl-3.0.html GNU General Public License v3.0
 */
if ( ! defined( 'ABSPATH' ) ) { exit; // Exit if accessed directly
}
?>

<?php $current_user = wp_get_current_user(); ?>

<div class="wmlf-widget wmlf-widget-avatar">
	<?php echo get_avatar( $current_user->ID, 250, 'mysteryman', false, array(
		'height' => 250,
		'width' => 250,
		'class' => '',
	) ); ?>
</div>