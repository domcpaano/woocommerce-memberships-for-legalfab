<?php
/**
 * WooCommerce Memberships
 *
 * @author    Dominique A. Mariano
 * @copyright Copyright (c) 2014-2016, Dominique A. Mariano
 * @license   http://www.gnu.org/licenses/gpl-3.0.html GNU General Public License v3.0
 */
if ( ! defined( 'ABSPATH' ) ) { exit; // Exit if accessed directly
}

// Get user details
$user = wp_get_current_user();
$user_id = $user->ID;

// Get user memberships
$user_memberships = wc_memberships_get_user_memberships( $user_id );

// Determine the member since date. Earliest membership wins!
$member_since = null;

if ( ! empty( $user_memberships ) ) {
	foreach ( $user_memberships as $user_membership ) {

		if ( ! $member_since || $member_since > $user_membership->get_local_start_date( 'timestamp' ) ) {
			$member_since = $user_membership->get_local_start_date( 'timestamp' );
		}
	}
}
?>
<div class="wmlf-widget wmlf-widget-contacts wmlf-widget-text">
	
	<h2><?php echo esc_html( $user->display_name ); ?><a title="" href="<?php echo wc_get_endpoint_url( 'edit-account' ); ?>" class="edit"><i class="wmlf-widget__edit-icon"><?php wmlf_get_svg('edit'); ?></i></a></h2>

	<p class="wmlf-widget-contacts__contact-details">
		<a href="mailto:<?php echo esc_attr( $user->user_email ); ?>" class="member-email"><?php echo esc_html( $user->user_email ); ?></a>
		<br />
		<?php if ( $member_since ) : ?>
			<span class="member-since">
				<?php printf( /* translators: %s - date */
					esc_html__( 'Member since %s', 'woocommerce-memberships' ), date_i18n( wc_date_format(), $member_since ) ); ?>
			</span>
		<?php endif; ?>
	</p>
	
</div>