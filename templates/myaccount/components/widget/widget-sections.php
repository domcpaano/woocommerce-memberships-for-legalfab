<?php
/**
 * WooCommerce Memberships
 *
 * @author    Dominique A. Mariano
 * @copyright Copyright (c) 2014-2016, Dominique A. Mariano
 * @license   http://www.gnu.org/licenses/gpl-3.0.html GNU General Public License v3.0
 */
if ( ! defined( 'ABSPATH' ) ) { exit; // Exit if accessed directly
}
?>

<?php if ( ! empty( $sections ) && is_array( $sections ) ) : ?>

	<?php $classes = ( empty( $classes ) ) ? array() : $classes; ?>

	<?php if ( is_string( $classes ) ) {
		$classes = explode( ' ', $classes );
	} else {
		$classes = (array) $classes;
	} ?>

	<?php $classes = array_unique( array_merge( array(
		'wmlf-widget-sections_reverse-compact-on-desktop',
		'wmlf-widget-sections_compact-before-desktop',
		'wmlf-widget-sections_theme_dashboard-before-desktop',
	), $classes ) ); ?>

	<ul class="wmlf-widget wmlf-widget-sections <?php echo esc_attr( implode( ' ', $classes ) ); ?>">
		
		<?php if ( empty( $sections_hierarchy ) || ! is_array( $sections_hierarchy ) ) {
				$sections_hierarchy = $sections;

} ?>

		<?php foreach ( $sections_hierarchy as $section => $sub_section ) : ?>

			<?php if ( ! empty( $sections_hierarchy[ $section ] ) || array_key_exists( $section, $sections ) ) : ?>
			
				<?php
					$has_sub_section 		= is_array( $sub_section ) && ! empty( $sub_section );
					$has_active_sub_section	= $has_sub_section && in_array( $current_section, $sub_section );
					$is_section_active 		= $current_section === $section;

					$classes = array();

				if ( $has_active_sub_section ) {
					$classes[] = 'wmlf-widget-sections__area_has-active-sub-area';
				}

				if ( $is_section_active ) {
					$classes[] = 'wmlf-widget-sections__area_is-active';
				}
				?>
				
				<li role="presentation" class="wmlf-widget-sections__area <?php echo esc_attr( implode( ' ', $classes ) ); ?>">

					<?php
						$header_name = ( is_string( $sub_section ) ) ? $sub_section : $section;
					if ( ! empty( $section_headings ) && is_array( $section_headings ) && isset( $section_headings[ $section ] ) ) {
						$header_name = $section_headings[ $section ];
					}
					?>

					<?php
						// Determine URL depending on whether we are on a membership area page or not.
						$url = '';
						if ( isset( $customer_membership ) && is_object( $customer_membership ) && method_exists( $customer_membership, 'get_plan_id' ) ) {
							$url = wc_memberships_get_members_area_url( $customer_membership->get_plan_id(), $section );
						} else {
							$url = wc_memberships_get_account_details_area_url( $section );
						}
					?>
					
					<a class="wmlf-widget-sections__name wmlf-widget-sections__header-name wmlf-widget-sections__link wmlf-widget-sections__area-link" href="<?php echo esc_url( $url ); ?>">
						<?php echo esc_html( $header_name ); ?>
							
						<?php if ( $is_section_active || $has_active_sub_section ) : ?>
							<i class="arrow-left vertical-center"></i>
						<?php endif; ?>
					</a>

					<span class="wmlf-widget-sections__name wmlf-widget-sections__header-name"><?php echo esc_html( $header_name ); ?>
						<?php if ( $is_section_active || $has_active_sub_section ) : ?>
							<i class="arrow-left vertical-center"></i>
						<?php endif; ?>
					</span>

					<?php if ( $has_sub_section ) : ?>

						<?php
							$classes = array();

						if ( in_array( $current_section, $sub_section ) ) {
							$classes[] = 'wmlf-widget-sections__sub-areas_is-active';
							$classes[] = 'wmlf-widget-sections__sub-areas_is-active_is-hidden-on-desktop';
						}
						?>

						<ul class="wmlf-widget-sections__sub-areas <?php echo esc_attr( implode( ' ', $classes ) ); ?>">
						<?php foreach ( $sub_section as $index => $sub_sub_section ) : ?>
						
							<li role="presentation" class="wmlf-widget-sections__sub-area">	
								<?php $name = $sub_sub_section; ?>

								<?php if ( is_array( $section_labels ) || isset( $section_labels[ $sub_sub_section ] ) ) {
									$name = $section_labels[ $sub_sub_section ];
} ?>

								<?php
									$url = '';

									if ( isset( $customer_membership ) && is_object( $customer_membership ) && method_exists( $customer_membership, 'get_plan_id' ) ) {
										$url = wc_memberships_get_members_area_url( $customer_membership->get_plan_id(), $sub_sub_section );
									} else {
										$url = wc_memberships_get_account_details_area_url( $sub_sub_section );
									}
								?>

								<a class="wmlf-widget-sections__link wmlf-widget-sections__area-link wmlf-widget-sections__sub-area-link" href="<?php echo esc_url( $url ); ?>">
									<span class="wmlf-widget-sections__name"><?php echo esc_html( $name ); ?></span>

									<?php // Output badges if we have any. ?>
									<?php if ( ! empty( $section_badges ) && in_array( $sub_sub_section, $section_badges ) ) :  ?>
										<span class="badge"><?php echo esc_html( wc_get_badge_count( $sub_sub_section ) ); ?></span>
									<?php endif; ?>
								</a>

							</li><!-- wmlf-widget-sections__sub-area -->
						<?php endforeach; ?>
						</ul><!-- .wmlf-widget-sections__sub-areas -->
					<?php endif; ?>
				</li><!-- wmlf-widget-sections__area -->

			<?php endif; ?>
		<?php endforeach; ?>

	</ul><!-- .wmlf-widget .wmlf-widget-sections .wmlf-widget-sections_compact -->

<?php endif; ?>
