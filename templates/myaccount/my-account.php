<?php
/**
 * My Account page
 *
 * This template can be overridden by copying it to yourtheme/woocommerce/myaccount/my-account.php.
 *
 * HOWEVER, on occasion WooCommerce will need to update template files and you (the theme developer).
 * will need to copy the new files to your theme to maintain compatibility. We try to do this.
 * as little as possible, but it does happen. When this occurs the version of the template file will.
 * be bumped and the readme will list any important changes.
 *
 * @see 	    http://docs.woothemes.com/document/template-structure/
 * @author 		WooThemes
 * @package 	WooCommerce/Templates
 * @version     2.0.0
 */

if ( ! defined( 'ABSPATH' ) ) {
	exit; // Exit if accessed directly
}
?>

<div class="wmlf">
	<header class="wmlf-site-header" role="banner">
		<?php wmlf_get_header(); ?>		
	</header>

	<div class="wmlf-app-content">
		<div class="wmlf-app-content-area">	
			<div class="wmlf-app-main">
				<?php wc_print_notices(); ?>

				<?php
					$account_areas = array_keys( wc_memberships_get_account_details_area_sections() );

					wmlf_get_tabs( array(
						'tabs'  => $account_areas,
						'current_section' => 'my-account-memberships',
						'section_labels' => wc_memberships_get_account_details_area_sections(),
					) );
				?>

				<div id="wc-memberships-members-area-section" class=""  data-page="1">

					<?php do_action( 'woocommerce_before_my_account' ); ?>
					
					<?php do_action( 'woocommerce_after_my_account' ); ?>
				</div>
			</div><!-- .wmlf-app-main -->	
		</div><!-- .wmlf-app-content-area -->	

		<aside class="wmlf-app-widget-area">
			<?php wmlf_get_widget( 'profile-badge' ); ?>

			<?php
				wmlf_get_widget( 'sections', array(
					'sections'				=> wc_memberships_get_account_details_area_sections(),
					'current_section' => 'my-account-memberships',
					'classes'					=> 'wmlf-widget-sections_hide-on-desktop',
				) );
			?>
			
			<?php wmlf_get_widget( 'addresses' ); ?>
		</aside><!-- .wmlf-app-widget-area -->
	</div><!-- .wmlf-app-content -->
</div><!-- .wmlf -->










