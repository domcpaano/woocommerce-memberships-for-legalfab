<?php
/**
 * My Orders
 *
 * Shows recent orders on the account page.
 *
 * This template can be overridden by copying it to yourtheme/woocommerce/myaccount/my-downloads.php.
 *
 * HOWEVER, on occasion WooCommerce will need to update template files and you (the theme developer).
 * will need to copy the new files to your theme to maintain compatibility. We try to do this.
 * as little as possible, but it does happen. When this occurs the version of the template file will.
 * be bumped and the readme will list any important changes.
 *
 * @see 	    http://docs.woothemes.com/document/template-structure/
 * @author 		WooThemes
 * @package 	WooCommerce/Templates
 * @version     2.0.0
 */

if ( ! defined( 'ABSPATH' ) ) {
	exit; // Exit if accessed directly
}

$my_orders_columns = apply_filters( 'woocommerce_memberships_my_account_my_downloads_columns', array(
	'download_url'  		=> __( 'File', 'woocommerce' ),
	'downloads_remaining'	=> __( 'Downloads Remaining', 'woocommerce' ),
	'access_expires'  		=> __( 'Access Expires', 'woocommerce' ),
) );

if ( $downloads = WC()->customer->get_downloadable_products() ) : ?>

	<?php do_action( 'woocommerce_before_available_downloads' ); ?>

	<table class="digital-downloads shop_table shop_table_responsive">
		<thead>
			<tr>
				<?php foreach ( $my_orders_columns as $column_id => $column_name ) : ?>
					<th><?php echo esc_html( $column_name ); ?></th>
				<?php endforeach; ?>

				<?php unset( $column_id, $column_name ); ?>
			</tr>
		</thead>
		<tbody>
			<?php foreach ( $downloads as $download ) : ?>
				<tr>
					<?php do_action( 'woocommerce_available_download_start', $download ); ?>
					<?php foreach ( $my_orders_columns as $column_id => $column_name ) : ?>
						<td>
						<?php
						if ( 'download_url' === $column_id ) {
							echo apply_filters( 'woocommerce_available_download_link', '<a href="' . esc_url( $download['download_url'] ) . '">' . $download['download_name'] . '</a>', $download );
						} elseif ( 'downloads_remaining' === $column_id ) {
							if ( is_numeric( $download['downloads_remaining'] ) ) {
								echo apply_filters( 'woocommerce_available_download_count', '<span class="count">' . sprintf( _n( '%s download remaining', '%s downloads remaining', $download['downloads_remaining'], 'woocommerce' ), $download['downloads_remaining'] ) . '</span> ', $download );
							} else {
								echo esc_html( __( 'Unlimited', 'woocommerce-memberships' ) );
							}
						} elseif ( 'access_expires' === $column_id ) {
							if ( ! empty( $download['access_expires'] ) ) {
								echo $download['access_expires'];
							} else {
								echo esc_html( __( 'Unlimited', 'woocommerce-memberships' ) );
							}
						}
						?>
						</td>
					<?php endforeach; ?>
					<?php do_action( 'woocommerce_available_download_end', $download ); ?>
				</tr>
			<?php endforeach; ?>
		</tbody>
	</table>

	<?php do_action( 'woocommerce_after_available_downloads' ); ?>

<?php endif; ?>
