<?php
/**
 * WooCommerce Memberships
 *
 * This source file is subject to the GNU General Public License v3.0
 * that is bundled with this package in the file license.txt.
 * It is also available through the world-wide-web at this URL:
 * http://www.gnu.org/licenses/gpl-3.0.html
 * If you did not receive a copy of the license and are unable to
 * obtain it through the world-wide-web, please send an email
 * to license@skyverge.com so we can send you a copy immediately.
 *
 * DISCLAIMER
 *
 * Do not edit or add to this file if you wish to upgrade WooCommerce Memberships to newer
 * versions in the future. If you wish to customize WooCommerce Memberships for your
 * needs please refer to http://docs.woothemes.com/document/woocommerce-memberships/ for more information.
 *
 * @package   WC-Memberships/Templates
 * @author    SkyVerge
 * @copyright Copyright (c) 2014-2016, SkyVerge, Inc.
 * @license   http://www.gnu.org/licenses/gpl-3.0.html GNU General Public License v3.0
 */


if ( ! defined( 'ABSPATH' ) ) { exit; // Exit if accessed directly
}
/**
 * Renders the content restricted to the membership in the my account area.
 *
 * @param WC_Memberships_User_Membership $customer_membership User Membership object
 * @param WP_Query $restricted_content Query results of posts and custom post types restricted to the membership
 * @param int $user_id The current user ID
 *
 * @version 1.5.4
 * @since 1.4.0
 */
?>


<?php // echo esc_html( apply_filters( 'wc_memberships_members_area_my_membership_content_title', __( 'My Membership Content', 'woocommerce-memberships' ) ) ); ?>

<?php // wc_memberships()->frontend->render_members_area_tabs(); ?>



<?php do_action( 'wc_memberships_before_members_area', 'my-membership-content' ); ?>

<?php if ( empty( $restricted_content->posts ) ) : ?>

	<?php wmlf_get_template_component( 'card', array(
		'title'		=> 'No scheduled content',
		'content' => "No content scheduled for this membership.",
	) ); ?>

<?php else : ?>

	<?php foreach ( $restricted_content->posts as $member_post ) : ?>

		<?php

		if ( ! $member_post instanceof WP_Post ) {
			continue;
		}

		// Determine if the content is currently accessible or not
		$can_view_content = wc_memberships_user_can( $user_id, 'view', array( 'post' => $member_post->ID ) );
		$view_start_time  = wc_memberships_get_user_access_start_time( $user_id, 'view', array( 'post' => $member_post->ID ) );
		?>
		
		<?php if ( ! $can_view_content ) : ?>
		<?php wmlf_get_template_component( 'card-article.php', array(
			'member_post' => $member_post,
		) ); ?>
		<?php endif; ?>
		
	<?php endforeach; ?>

<?php endif; ?>

<?php do_action( 'wc_memberships_after_members_area', 'my-membership-content' ); ?>
