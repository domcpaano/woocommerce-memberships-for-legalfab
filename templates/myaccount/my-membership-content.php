<?php
if ( ! defined( 'ABSPATH' ) ) { exit; // Exit if accessed directly
}
?>

<?php do_action( 'wc_memberships_before_members_area', 'my-membership-content' ); ?>

<?php if ( empty( $restricted_content->posts ) ) : ?>

	<?php wmlf_get_template_component( 'card', array(
		'title'		=> 'No content',
		'content'	 => "No content assigned to this membership.",
	) ); ?>

<?php else : ?>
	
	<?php echo wc_memberships_get_members_area_page_links( $customer_membership->get_plan(), 'my-membership-content', $restricted_content ); ?>

	<?php foreach ( $restricted_content->posts as $member_post ) : ?>

		<?php

		if ( ! $member_post instanceof WP_Post ) {
			continue;
		}

		// Determine if the content is currently accessible or not
		$can_view_content = wc_memberships_user_can( $user_id, 'view', array( 'post' => $member_post->ID ) );
		$view_start_time  = wc_memberships_get_user_access_start_time( $user_id, 'view', array( 'post' => $member_post->ID ) );
		?>
		
		<?php wmlf_get_template_component( 'card-article.php', array(
			'member_post' => $member_post,
			'post_controls' => array(
				'view' => array(),
				'view-revisions' => array(),
				'view-stats' => array(),
				'bookmark' => array(),
			),
			'post_meta_controls' => array(
				'vote-up' => array(),
				'vote-down' => array(),
			),
		) ); ?>
		
	<?php endforeach; ?>

	<?php echo wc_memberships_get_members_area_page_links( $customer_membership->get_plan(), 'my-membership-content', $restricted_content ); ?>

<?php endif; ?>

<?php do_action( 'wc_memberships_after_members_area', 'my-membership-content' ); ?>
