<?php
/**
 * View Order
 *
 * Shows the details of a particular order on the account page.
 *
 * This template can be overridden by copying it to yourtheme/woocommerce/myaccount/view-order.php.
 *
 * HOWEVER, on occasion WooCommerce will need to update template files and you (the theme developer).
 * will need to copy the new files to your theme to maintain compatibility. We try to do this.
 * as little as possible, but it does happen. When this occurs the version of the template file will.
 * be bumped and the readme will list any important changes.
 *
 * @see 	    http://docs.woothemes.com/document/template-structure/
 * @author    WooThemes
 * @package   WooCommerce/Templates
 * @version   2.2.0
 */

if ( ! defined( 'ABSPATH' ) ) {
	exit; // Exit if accessed directly
}
?>

<div class="wmlf">
	<header class="wmlf-site-header" role="banner">
		<?php wmlf_get_header(); ?>		
	</header>

	<div class="wmlf-app-content">
		<div class="wmlf-app-content-area">	
			<div class="wmlf-app-main">
				<?php wc_print_notices(); ?>

				<p class="order-info"><?php printf( __( 'Order #<mark class="order-number">%s</mark> was placed on <mark class="order-date">%s</mark> and is currently <mark class="order-status">%s</mark>.', 'woocommerce' ), $order->get_order_number(), date_i18n( get_option( 'date_format' ), strtotime( $order->order_date ) ), wc_get_order_status_name( $order->get_status() ) ); ?></p>

				<?php if ( $notes = $order->get_customer_order_notes() ) : ?>
					<?php
						wmlf_get_tabs( array(
							'tabs' => __( 'Order Updates', 'woocommerce' ),
						) );
					?>
					
					<ol class="commentlist notes">
						<?php foreach ( $notes as $note ) : ?>
						<li class="comment note">
							<div class="comment_container">
								<div class="comment-text">
									<p class="meta"><?php echo date_i18n( __( 'l jS \o\f F Y, h:ia', 'woocommerce' ), strtotime( $note->comment_date ) ); ?></p>
									<div class="description">
										<?php echo wpautop( wptexturize( $note->comment_content ) ); ?>
									</div>
					  				<div class="clear"></div>
					  			</div>
								<div class="clear"></div>
							</div>
						</li>
						<?php endforeach; ?>
					</ol>
					<?php endif; ?>

				<?php do_action( 'woocommerce_view_order', $order_id ); ?>

			</div><!-- .wmlf-app-main -->	
		</div><!-- .wmlf-app-content-area -->	

		<aside class="wmlf-app-widget-area">
			<?php wmlf_get_widget( 'profile-badge' ); ?>
	
			<?php
				wmlf_get_widget( 'sections', array(
					'sections'				=> wc_memberships_get_account_details_area_sections(),
					'current_section' 		=> 'my-account-orders',
					'classes'				=> 'wmlf-widget-sections_hide-on-desktop',
				) );
			?>

			<?php wmlf_get_widget( 'addresses' ); ?>
		</aside><!-- .wmlf-app-widget-area -->	
	</div><!-- .wmlf-app-content -->
</div><!-- .wmlf -->
